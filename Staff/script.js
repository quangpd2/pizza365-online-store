
"use strict";
$(document).ready(function(){

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gDataOrders = [];// Khai báo biến mảng(biến toàn cục) để chứa thông tin đơn hàng

const gORDER = ["orderCode", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];

//Khai báo các cột cửa DataTable
const gCOLUMN_ORDER_CODE = 0 ; 
const gCOLUMN_KICH_CO = 1;
const gCOLUMN_LOAI_PIZZA = 2;
const gCOLUMN_ID_LOAI_NUOC_UONG = 3;
const gCOLUMN_THANH_TIEN = 4;
const gCOLUMN_HO_TEN = 5;
const gCOLUMN_SO_DIEN_THOAI = 6;
const gCOLUMN_TRANG_THAI = 7;
const gCOLUMN_ACTION = 8;

//Định nghĩa table - chưa có data
$("#table-order").DataTable({
    columns :[
        {data : gORDER[gCOLUMN_ORDER_CODE]},
        {data : gORDER[gCOLUMN_KICH_CO]},
        {data : gORDER[gCOLUMN_LOAI_PIZZA]},
        {data : gORDER[gCOLUMN_ID_LOAI_NUOC_UONG]},
        {data : gORDER[gCOLUMN_THANH_TIEN]},
        {data : gORDER[gCOLUMN_HO_TEN]},
        {data : gORDER[gCOLUMN_SO_DIEN_THOAI]},
        {data : gORDER[gCOLUMN_TRANG_THAI]},
        {data : gORDER[gCOLUMN_ACTION]},
    ],
    columnDefs :[
        {
            targets : gCOLUMN_ACTION,
            defaultContent : '<i type="button" class="fas fa-edit btn-edit" style="color:blue"></i>&nbsp; <i type="button" class="fas fa-trash-alt btn-delete" style="color:red"></i>'
        }
    ]

});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

//Gán sự kiện click cho nút lọc
$("#btn-filter").on("click",function(){
    onBtnFilterClick();
})

//Gán sự kiện click cho nút Create Order
$("#btn-create-order").on("click",function(){
    onBtnCreateOrderClick();
})

//Gán sự kiện click cho nút Edit 
$("#table-order").on("click",".btn-edit",function(){
    onBtnEditClick(this);
});

//Gán sự kiện click cho nút Delete
$("#table-order").on("click",".btn-delete",function(){
    onBtnDeleteClick(this);
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Hàm xử lý sự kiện tải trang
function onPageLoading(){
    $.ajax({
        url : "http://203.171.20.210:8080/devcamp-pizza365/orders",
        type : 'GET',
        dataType : 'json',
        success : function(paramResponse){
            //Khi lấy dữ liệu cho gDataOrders ta lọc và loại bỏ các giá trị có loại pizza = null.
            gDataOrders = paramResponse.filter(function(item){
                return item.loaiPizza != null;
            })
            console.log(gDataOrders);
            loadDataToTable(gDataOrders);
        },
        error : function(paramErr){
            console.log(paramErr.responseText);
        }
    });
    callAjaxGetDrink();

}
//Hàm xử lý khi click vào nút Lọc
function onBtnFilterClick(){
    console.log("Nút lọc được click");
    var vStatus = $("#sel-status").val();
    var vPizzaType = $("#sel-pizza").val();
    var vDataFilter = gDataOrders.filter(function(paramOrder,index){
        return (vStatus === "all" || vStatus.toUpperCase() === paramOrder.trangThai.toUpperCase())
        && (vPizzaType === "all" || vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase());
    })
    loadDataToTable(vDataFilter);
}

//Hàm xử lý khi click vào nút Create Order
function onBtnCreateOrderClick(){
    $("#create-order-modal").modal("show");
    //Gán sự kiện change cho select Pizza Size
    $("#select-pizza-size").on("change",function(){
        onSelectPizzaSizechange(this);
    });
    //Gán sự kiện click cho nút Tạo Đơn
    $("#btn-confirm-create").on("click",function(){
        onBtnTaoDonClick();
    });
    //Gán sự kiện click cho nút Hủy
    $("#btn-cancel-create").on("click",function(){
        onBtnHuyDonClick();
    })
}

//Hàm xử lý khi click vào nút Tạo đơn trên modal Create Order
function onBtnTaoDonClick(){
    var vObjectRequest = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    };
    //B1 : Thu Thập dữ liệu
    thuThapDuLieuModalCreateOrder(vObjectRequest);    
    
    //B2 : Kiểm tra dữ liệu
    var vDuLieuHopLe = kiemTraDuLieuModalCreateOrder(vObjectRequest);
    if(vDuLieuHopLe == true){
        console.log(vObjectRequest);
        //B3 : Insert User
        $.ajax({
            url : "http://203.171.20.210:8080/devcamp-pizza365/orders",
            type : 'POST',
            contentType : 'application/json;charset=UTF-8',
            data : JSON.stringify(vObjectRequest),
            success : function(paramResponse){
                console.log(paramResponse);
                alert("BẠN ĐÃ TẠO ĐƠN THÀNH CÔNG !");
                window.location.href = "Staff.html";
            },
            error : function(paramErr){
                console.log(paramErr.responseText);
            }
        })
    }
    
}
//Hàm xử lý khi click vào nút Hủy
function onBtnHuyDonClick(){
    $("#create-order-modal").modal("hide");
    resetDataFromModalCreate();
}

//Hàm xử lý khi click vào nút Edit 
function onBtnEditClick(paramButton){
    console.log("Button edit click");
    var vTableOrder = $("#table-order").DataTable();
    var vRowClick = $(paramButton).closest("tr");
    var vDataRow = vTableOrder.row(vRowClick).data();
    var vOrderCode = vDataRow.orderCode;
    var vId = vDataRow.id;
    console.log(vId);
    console.log("OrderCode : " + vOrderCode);
    getAjaxOrderDetail(vOrderCode);
    $("#update-order-modal").modal("show");
    //Gán sự kiện click cho nút Thay đổi trạng thái 
    $("#btn-confirm-status").on("click",function(){
        onChangeStatusClick(vId);
    });
    //Gán sự kiện click cho nút Hủy trong modal update order
    $("#btn-cancel-status").on("click",function(){
        onBtnCancelChangeStatus();
    })
    
}

//Hàm xử lý khi click vào nút Delete
function onBtnDeleteClick(paramButton){
    console.log("Button delete click");
    var vTableOrder = $("#table-order").DataTable();
    var vRowClick = $(paramButton).closest("tr");
    var vDataRow = vTableOrder.row(vRowClick).data();
    var vOrderCode = vDataRow.orderCode;
    var vId = vDataRow.id;
    console.log(vId);
    console.log("OrderCode : " + vOrderCode);
    $("#delete-order-modal").modal("show");
    //gán sự kiện click cho nút đồng ý
    $("#btn-confirm-delete-order").on("click",function(){
        onBtnConfirmDeleteOrder(vId);
    })
}

//Hàm xử lý khi click vào nút đồng ý trên modal delete order
function onBtnConfirmDeleteOrder(paramId){
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramId,
        type :'DELETE',
        success : function(res){
            alert("Xóa thành công !");
            window.location.href = "Staff.html"
        },
        error : function(err){
            console.log(res.responseText);
            alert("Xóa thất bại");
        }

    })
}
//Hàm xử lý khi click vào nút Thay đổi trạng thái
function onChangeStatusClick(paramId){
    var vObjectRequest = {
        trangThai : ""
    }
    //B1 : Thu thập trạng thái 
    thuThapTrangThaiModalUpdate(vObjectRequest);
    //B2 : kiểm tra - bỏ qua
    //B3 : Insert Trạng Thái Mới 
    callAjaxUpdateOrder(vObjectRequest,paramId);
    alert("Update Trạng Thái Thành Công");
    window.location.href = "Staff.html";
    
}

//Hàm xử lý khi click vào nút Hủy trong modal update order
function onBtnCancelChangeStatus(){
    $("#update-order-modal").modal("hide");
    //Reset lại các giá trị
    $("#inp-update-order-code").val("");
    $("#select-update-pizza-size").val("");
    $("#inp-update-duong-kinh").val("");
    $("#inp-update-suon").val("");
    $("#inp-update-salad").val("");
    $("#select-update-pizza-type").val("");
    $("#inp-update-id-voucher").val("");
    $("#inp-update-giam-gia").val("");
    $("#inp-update-thanh-tien").val("");
    $("#select-update-drink").val("");
    $("#inp-update-so-luong-drink").val("");
    $("#inp-update-full-name").val("");
    $("#inp-update-email").val("");
    $("#inp-update-phone").val("");
    $("#inp-update-address").val("");
    $("#inp-update-message").val("");
    $("#inp-update-order-day").val("");
    $("#inp-update-repair-day").val("");
    $("#select-update-status").val("");
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//Hàm call ajax update order
function callAjaxUpdateOrder(paramObject,paramId){
    $.ajax({
        url : "http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramId,
        type : 'PUT',
        contentType : 'application/json;charset=UTF-8',
        data : JSON.stringify(paramObject),
        success : function(paramResponse){
            console.log(paramResponse);
        },
        error : function(paramErr){
            console.log(paramErr.responseText);
        }
    })
}
//Hàm thu thập trạng thái trên form modal Update 
function thuThapTrangThaiModalUpdate(paramResponse){
    paramResponse.trangThai = $("#select-update-status").val();
}
//Hàm Call Ajax lấy orderDetail qua ordercode
function getAjaxOrderDetail(paramOrderCode){
    $.ajax({
        url : "http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramOrderCode,
        type : 'GET',
        dataType : 'json',
        success : function(paramResponse){
            console.log(paramResponse);
            loadDataToModalUpdate(paramResponse);
        },
        error : function(paramErr){
            console.log(paramErr.responseText);
        }
    })
}
//Hàm thu thập dữ liệu trên Modal CreateOrder
function thuThapDuLieuModalCreateOrder(paramObject){
    paramObject.kichCo = $("#select-pizza-size").val();
    paramObject.duongKinh = $("#inp-duong-kinh").val();
    paramObject.suon = $("#inp-suon").val();
    paramObject.salad = $("#inp-salad").val();
    paramObject.loaiPizza = $("#select-pizza-type").val();
    paramObject.idVourcher = $("#inp-id-voucher").val();
    paramObject.idLoaiNuocUong =$("#select-drink").val();
    paramObject.soLuongNuoc =$("#inp-so-luong-drink").val();
    paramObject.hoTen = $("#inp-full-name").val();
    paramObject.thanhTien = $("#inp-thanh-tien").val();
    paramObject.email = $("#inp-email").val();
    paramObject.soDienThoai =$("#inp-phone").val();
    paramObject.diaChi = $("#inp-address").val();
    paramObject.loiNhan =$("#inp-message").val();
}
//Hàm kiểm tra dữ liệu trên Modal CreateOrder
function kiemTraDuLieuModalCreateOrder(paramObject){
    if(paramObject.kichCo == 0){
        alert("Bạn chưa chọn Pizza Size !");
        return false;
    }

    if(paramObject.loaiPizza == 0){
        alert("Bạn chưa chọn Loại Pizza !");
        return false;
    }

    if(paramObject.idLoaiNuocUong == 0){
        alert("Bạn hãy chọn một đồ uống bất kỳ");
        return false;
    }

    if(paramObject.hoTen == ""){
        alert("Hãy nhập họ và tên");
        return false;
    }

    var vCheckEmail = kiemTraEmail(paramObject.email);
    if(vCheckEmail == false){
        return false;
    }

    if(paramObject.soDienThoai == ""){
        alert("Bạn chưa nhập số điện thoại");
        return false;
    }

    if(isNaN(paramObject.soDienThoai)){
        alert("Điện thoại phải là số ");
        return false;
    }

    if(paramObject.diaChi == ""){
        alert(" Hãy nhập địa chỉ để thuận tiện cho việc giao hàng");
        return false;
    }

    return true;
}
//Hàm kiểm tra Email 
function kiemTraEmail(paramEmail){
    var vEmail = paramEmail;
    // Nếu email nhập không chứa ký tự @ 
    if(vEmail.includes("@") == false){
    alert("Email phải chưa ký tự @");
    return false;
    }

    // Kiểm tra trước và sau ký tự @ có ký tự nào hay không
    if(vEmail.startsWith("@") == true || vEmail.endsWith("@") == true){
    alert("Email không được bắt đầu và kết thúc bằng ký tư '@' ");
    return false;
    }
    return true;
}
//Hàm đổ dữ liệu vào bảng
function loadDataToTable(paramOrderObj){
    var vTable = $("#table-order").DataTable();
    vTable.clear();
    vTable.rows.add(paramOrderObj);
    vTable.draw();
}

//Hàm Call Ajax lấy dữ liệu drink từ trên server về
function callAjaxGetDrink(){
    $.ajax({
        url :"http://203.171.20.210:8080/devcamp-pizza365/drinks",
        type : 'GET',
        dataType : 'json',
        success : function(paramResponse){
            console.log(paramResponse);
            loadDataToSelectDrink(paramResponse);
        },
        error : function(paramErr){
            console.log(paramErr.responseText);
        }
    })
}
//Hàm đổ dữ liệu vào select Drink
function loadDataToSelectDrink(paramDrink){
    $("<option>",{
        value : 0,
        text : "Chọn loại đồ uống"
    }).appendTo($("#select-drink"));

    for(var bI = 0 ; bI < paramDrink.length ; bI ++){
        $("<option>",{
            value : paramDrink[bI].maNuocUong,
            text : paramDrink[bI].tenNuocUong
        }).appendTo($("#select-drink"));
    }
}
//Hàm sử lý khi chọn select Pizza size
function onSelectPizzaSizechange(paramSelect){
    var PizzaSizeSeleted = $(paramSelect).val();
    if(PizzaSizeSeleted == 0){
        $("#inp-duong-kinh").val("");
        $("#inp-suon").val("");
        $("#inp-salad").val("");
        $("#inp-so-luong-drink").val("");
        $("#inp-thanh-tien").val("");
    }
    else if(PizzaSizeSeleted == "S"){
        $("#inp-duong-kinh").val("20");
        $("#inp-suon").val("2");
        $("#inp-salad").val("200");
        $("#inp-so-luong-drink").val("2");
        $("#inp-thanh-tien").val("150000");
    }
    else if(PizzaSizeSeleted == "M"){
        $("#inp-duong-kinh").val("25");
        $("#inp-suon").val("4");
        $("#inp-salad").val("300");
        $("#inp-so-luong-drink").val("3");
        $("#inp-thanh-tien").val("200000");
    }
    else if(PizzaSizeSeleted == "L"){
        $("#inp-duong-kinh").val("30");
        $("#inp-suon").val("8");
        $("#inp-salad").val("500");
        $("#inp-so-luong-drink").val("4");
        $("#inp-thanh-tien").val("250000");
    }
}
//hàm reset lại data Modal Create
function resetDataFromModalCreate(){
    $("#select-pizza-size").val("0");
    $("#inp-duong-kinh").val("");
    $("#inp-suon").val("");
    $("#inp-salad").val("");
    $("#select-pizza-type").val("0");
    $("#inp-id-voucher").val("");
    $("#select-drink").val("0");
    $("#inp-so-luong-drink").val("");
    $("#inp-full-name").val("");
    $("#inp-thanh-tien").val("");
    $("#inp-email").val("");
    $("#inp-phone").val("");
    $("#inp-address").val("");
    $("#inp-message").val("");
}
//hàm load dữ liệu vào modal update
function loadDataToModalUpdate(paramObject){
    $("#inp-update-order-code").val(paramObject.orderCode);
    $("#select-update-pizza-size").val(paramObject.kichCo);
    $("#inp-update-duong-kinh").val(paramObject.duongKinh);
    $("#inp-update-suon").val(paramObject.suon);
    $("#inp-update-salad").val(paramObject.salad);
    $("#select-update-pizza-type").val(paramObject.loaiPizza);
    $("#inp-update-id-voucher").val(paramObject.idVourcher);
    // $("#inp-update-thanh-tien").val(paramObject.thanhTien);
    $("#inp-update-giam-gia").val(paramObject.giamGia);
    var vThanhTien = paramObject.thanhTien - paramObject.giamGia;
    $("#inp-update-thanh-tien").val(vThanhTien);
    $("#select-update-drink").val(paramObject.idLoaiNuocUong);
    $("#inp-update-so-luong-drink").val(paramObject.soLuongNuoc);
    $("#inp-update-full-name").val(paramObject.hoTen);
    $("#inp-update-email").val(paramObject.email);
    $("#inp-update-phone").val(paramObject.soDienThoai);
    $("#inp-update-address").val(paramObject.diaChi);
    $("#inp-update-message").val(paramObject.loiNhan);
    $("#inp-update-order-day").val(paramObject.ngayTao);
    $("#inp-update-repair-day").val(paramObject.ngayCapNhat);
    $("#select-update-status").val(paramObject.trangThai);


}

})
